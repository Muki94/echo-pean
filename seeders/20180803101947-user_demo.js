'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('users', [{
        name: 'Muhamed Al-Naqli',
        username: 'Muki',
        password: '$2b$10$pBMN2j7wdjnmGwyWqMXCOeza1vS66HPJWt9U/Y3XdOIXrE1Hh3/uq',
        email: 'muhamed.alnaqli.aplab12@gmail.com',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        name: 'John Doe',
        username: 'John',
        password: '$2b$10$pBMN2j7wdjnmGwyWqMXCOeza1vS66HPJWt9U/Y3XdOIXrE1Hh3/uq',
        email: 'john@gmail.com',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        name: 'Selma Muminovic',
        username: 'Memki',
        password: '$2b$10$pBMN2j7wdjnmGwyWqMXCOeza1vS66HPJWt9U/Y3XdOIXrE1Hh3/uq',
        email: 'selma@gmail.com',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        name: 'Mersad Secic',
        username: 'Merso',
        password: '$2b$10$pBMN2j7wdjnmGwyWqMXCOeza1vS66HPJWt9U/Y3XdOIXrE1Hh3/uq',
        email: 'merso@hotmail.com',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('users', null, {});
  }
};