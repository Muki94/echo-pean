'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('messages', [{
        userId: 1,
        content: 'content 1',
        destination: 'Mostar',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        userId: 2,
        content: 'content 2',
        destination: 'Sarajevo',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        userId: 3,
        content: 'content 3',
        destination: 'Bihac',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        userId: 4,
        content: 'content 4',
        destination: 'Tuzla',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 1,
        content: 'content 5',
        destination: 'Konjic',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 2,
        content: 'content 6',
        destination: 'Beograd',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 3,
        content: 'content 7',
        destination: 'Zagreb',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 3,
        content: 'content 8',
        destination: 'New York',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 2,
        content: 'content 9',
        destination: 'Breza',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('messages', null, {});
  }
};