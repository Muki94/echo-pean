'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user_message_ratings', [{
        userId: 1,
        messageId: 1,
        liked: true,
        disliked: false,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        userId: 1,
        messageId: 1,
        liked: false,
        disliked: false,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        userId: 1,
        messageId: 1,
        liked: false,
        disliked: true,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        userId: 4,
        messageId: 3,
        liked: true,
        disliked: false,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 1,
        messageId: 3,
        liked: false,
        disliked: false,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 2,
        messageId: 2,
        liked: true,
        disliked: true,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 3,
        messageId: 1,
        liked: false,
        disliked: false,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 3,
        messageId: 1,
        liked: false,
        disliked: true,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 2,
        messageId: 1,
        liked: true,
        disliked: false,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_message_ratings', null, {});
  }
};