'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('comments', [{
        userId: 1,
        messageId: 1,
        content: 'comment content 1',
        destination: 'Mostar',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        userId: 2,
        messageId: 1,
        content: 'comment content 2',
        destination: 'Sarajevo',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        userId: 3,
        messageId: 3,
        content: 'comment content 3',
        destination: 'Bihac',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        userId: 4,
        messageId: 3,
        content: 'comment content 4',
        destination: 'Tuzla',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 1,
        messageId: 3,
        content: 'comment content 5',
        destination: 'Konjic',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 2,
        messageId: 2,
        content: 'comment content 6',
        destination: 'Beograd',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 3,
        messageId: 1,
        content: 'comment content 7',
        destination: 'Zagreb',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 3,
        messageId: 1,
        content: 'comment content 8',
        destination: 'New York',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 2,
        messageId: 1,
        content: 'comment content 9',
        destination: 'Breza',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('comments', null, {});
  }
};