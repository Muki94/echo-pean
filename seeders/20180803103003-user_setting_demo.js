'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user_settings', [{
        userId: 1,
        settingId: 1,
        value: true,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        userId: 1,
        settingId: 2,
        value: false,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        userId: 2,
        settingId: 1,
        value: true,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        userId: 2,
        settingId: 2,
        value: true,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 3,
        settingId: 1,
        value: true,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 3,
        settingId: 2,
        value: true,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 4,
        settingId: 1,
        value: true,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }, {
        userId: 4,
        settingId: 2,
        value: true,
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_settings', null, {});
  }
};