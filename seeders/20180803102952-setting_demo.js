'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('settings', [{
        name: 'Show location',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      },
      {
        name: 'Show name',
        createdAt: '2018-1-1',
        updatedAt: '2018-1-1'
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('settings', null, {});
  }
};