const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('./../models').user;
const config = require('../config/config.json');

module.exports = function (passport) {
    let options = {};
    options.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
    options.secretOrKey = config.test.secret;
    passport.use(new JwtStrategy(options, (jwt_payload, done) => {

        if (jwt_payload == null)
            return done('payload is empty!', null);

        User.findById(jwt_payload.id).then(user => {
            return done(null, user);
        })
    }));
}