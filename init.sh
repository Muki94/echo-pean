#!/bin/bash
#instaliraj dependencije
npm install 
#instaliraj sequelize cli za izvrsavanje sequelize koda
npm install -g sequelize-cli
#instaliraj sequelize ORM za code first pristup 
npm install -g sequelize
#instaliraj plugin za posgresql 
npm install -g pg 
#izvrsi kreirane migracije 
sequelize db:migrate
#izvrsi seed kako bi popunio tabele u bazi
sequelize db:seed:all
#pokreni server
node app



