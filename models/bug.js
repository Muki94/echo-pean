'use strict';
module.exports = (sequelize, DataTypes) => {
  var bug = sequelize.define('bug', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    }
  }, {});
  bug.associate = function (models) {
    // associations can be defined here
  };
  return bug;
};