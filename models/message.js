'use strict';
var Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    var message = sequelize.define('message', {
        content: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        destination: {
            type: DataTypes.STRING,
            allowNull: false
        },
        rating: Sequelize.VIRTUAL
    }, {});
    message.associate = function (models) {
        // associations can be defined here
        message.hasMany(models.comment);
        message.hasMany(models.user_message_rating);
        message.belongsTo(models.user, {
            foreignKey: 'userId'
        });
    };
    return message;
};