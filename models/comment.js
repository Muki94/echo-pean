'use strict';
module.exports = (sequelize, DataTypes) => {
    var comment = sequelize.define('comment', {
        content: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        destination: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {});
    comment.associate = function (models) {
        // associations can be defined here
        comment.belongsTo(models.message, {
            foreignKey: 'messageId'
        });
        comment.belongsTo(models.user, {
            foreignKey: 'userId'
        });
    };
    return comment;
};