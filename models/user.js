'use strict';
module.exports = (sequelize, DataTypes) => {
  var user = sequelize.define('user', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    username: DataTypes.STRING,
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: true,
        notEmpty: true
      }
    }
  }, {});
  user.associate = function (models) {
    // associations can be defined here
    user.hasMany(models.user_setting);
    user.hasMany(models.message);
    user.hasMany(models.user_message_rating);
    user.hasMany(models.comment);
  };
  return user;
};