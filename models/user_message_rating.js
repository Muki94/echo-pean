'use strict';
module.exports = (sequelize, DataTypes) => {
  var user_message_rating = sequelize.define('user_message_rating', {
    liked: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    disliked: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {});
  user_message_rating.associate = function (models) {
    // associations can be defined here
    user_message_rating.belongsTo(models.user, {
      foreignKey: 'userId'
    });
    user_message_rating.belongsTo(models.message, {
      foreignKey: 'messageId'
    });
  };
  return user_message_rating;
};