'use strict';
module.exports = (sequelize, DataTypes) => {
  var setting = sequelize.define('setting', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    }
  }, {});
  setting.associate = function (models) {
    // associations can be defined here
    setting.hasMany(models.user_setting);
  };
  return setting;
};