'use strict';
module.exports = (sequelize, DataTypes) => {
  var user_setting = sequelize.define('user_setting', {
    value: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {});
  user_setting.associate = function (models) {
    // associations can be defined here
    user_setting.belongsTo(models.user, {
      foreignKey: 'userId'
    });
    user_setting.belongsTo(models.setting, {
      foreignKey: 'settingId'
    });
  };
  return user_setting;
};