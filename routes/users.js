var bcrypt = require("bcrypt");
var passport = require("passport");
var config = require('../config/config.json');
var jwt = require('jsonwebtoken');

var express = require("express");
var router = express.Router();

var User = require("../models").user;
var Message = require("../models").message;
var Comment = require("../models").comment;
var UserSetting = require("../models").user_setting;

//get all users
router.get("/", passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    User.findAll({
            include: [Message, Comment],
            order: [
                ["id", "ASC"]
            ]
        })
        .then(users => {
            res.send(users).status(200);
        })
        .catch(error => {
            res.send(error).status(500);
        });
});

//get single users
router.get("/:id", passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    User.findById(req.params.id, {
            include: [Message, Comment]
        })
        .then(user => {
            res.send(user).status(200);
        })
        .catch(error => {
            res.send(error).status(500);
        });
});

//add new users
router.post("/", (req, res) => {
    console.log(req.body);
    //check if user with same username exists
    User.findOne({
            where: {
                username: {
                    $like: req.body.username
                }
            }
        })
        .then(result => {
            if (result != null)
                res.send("user with same username exists!").status(404);
            else {
                //if user with same username does not exist create new user
                bcrypt.genSalt(10, (error, salt) => {
                    bcrypt.hash(req.body.password, salt, (error, hash) => {
                        req.body.password = hash;

                        User.create(req.body)
                            .then(result => {
                                res.send(result).status(200);
                            })
                            .catch(error => {
                                res.send(error).status(500);
                            });
                    });
                });
            }
        })
        .catch(error => {
            res.send(error).status(500);
        });
});

//authenticate user
router.post("/authenticate", (req, res) => {
    const username = req.body.username;
    const password = req.body.password;


    User.findOne({
            where: {
                username: {
                    $like: username
                }
            }
        })
        .then(user => {
            if (user != null) {
                comparePassword(password, user.password, (error, isMatch) => {
                    if (isMatch) res.send(generateToken(user)).status(200);
                    else {
                        res.send({
                            success: false,
                            message: 'user does not exist!'
                        }).status(401);
                    }
                })
            } else {
                res.send({
                    success: false,
                    message: 'user does not exist!'
                }).status(401);
            }
        })
        .catch(error => {
            res.send(error).status(500);
        });
});

function comparePassword(password, hashPassword, callback) {
    bcrypt.compare(password, hashPassword, (error, isMatch) => {
        if (error) throw error;
        return callback(null, isMatch);
    });
}

function generateToken(user) {
    const token = jwt.sign(user.dataValues, config.test.secret, {});

    return {
        success: token != null ? true : false,
        message: token != null ? "user loged in!" : "you could not be authenticated!",
        token: 'JWT ' + token,
        user: user.dataValues
    };
}

//edit existing users
router.put("/:id", passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    User.update(req.body, {
            where: {
                id: req.params.id
            }
        })
        .then(result => {
            res.send(result).status(200);
        })
        .catch(err => {
            res.send(err).status(500);
        });
});

//delete user
router.delete("/:id", passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    User.destroy({
            where: {
                id: req.params.id
            }
        })
        .then(result => {
            if (result == 1) res.send("deleted!").status(200);
            else res.send("something went wrong!").status(404);
        })
        .catch(error => {
            res.send(error).status(500);
        });
});

//get all settings belonging to specific user
router.get("/getUserSettings/:userId", passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    User.findById(req.params.userId, {
            include: [UserSetting]
        })
        .then(userSettings => {
            res.send(userSettings).status(200);
        })
        .catch(error => {
            res.send(erro).status(500);
        });
});

//update user setting
router.put("/editUserSetting/:id", passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    UserSetting.update(req.body, {
            where: {
                id: req.params.id
            }
        })
        .then(result => {
            res.send(result).status(200);
        })
        .catch(error => {
            res.send(error).status(500);
        });
});

module.exports = router;