var express = require('express');
var passport = require('passport');
var router = express.Router();

var Comment = require('../models').comment;

//get all comments
router.get('/', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    Comment.findAll({
        order: [
            ['id', 'ASC']
        ]
    }).then(comments => {
        res.send(comments).status(200);
    }).catch(error => {
        res.send(error).status(500);
    });
});

//get single comment
router.get('/:id', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    Comment.findById(req.params.id).then(comment => {
        res.send(comment);
    }).catch(error => {
        res.send(error).status(500);
    });
});

//add comment to message
router.post('/', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    Comment.create(req.body).then(result => {
        res.send(result).status(200);
    }).catch(error => {
        res.send(error).status(500);
    });
});

//edit existing comment
router.put('/:id', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    Comment.update(req.body, {
        where: {
            id: req.params.id
        }
    }).then(result => {
        res.send(result).status(200);
    }).catch(error => {
        res.send(error).status(500);
    });
});

//delete comment
router.delete('/:id', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    Comment.destroy({
        where: {
            id: req.params.id
        }
    }).then(result => {
        if (result == 1)
            res.send('deleted!').status(200);
        else
            res.send('something went wrong!').status(404);
    }).catch(error => {
        res.send(error).status(500);
    });
});

module.exports = router;