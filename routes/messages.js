var express = require("express");
var passport = require('passport');
var router = express.Router();

var Message = require("../models").message;
var Comment = require("../models").comment;
var UserMessageRating = require("../models").user_message_rating;

//parameters
var content;
var destination;

//get all messages
router.get("/", passport.authenticate('jwt', {
  session: false
}), (req, res) => {

  content = validateSearchParameter(req.query.content) ? '%' + req.query.content + '%' : '%';
  destination = validateSearchParameter(req.query.destination) ? '%' + req.query.destination + '%' : '%';

  Message.findAll({
      include: [Comment, UserMessageRating],
      order: [
        ["id", "ASC"]
      ],
      where: {
        $or: [{
          content: {
            $like: content
          }
        }, {
          destination: {
            $like: destination
          }
        }]
      }
    })
    .then(messages => {
      setMessagesRating(messages);
      res.send(messages);
    })
    .catch(error => {
      res.send(error).status(500);
    });
});

//get single messages
router.get("/:id", passport.authenticate('jwt', {
  session: false
}), (req, res) => {

  Message.findById(req.params.id, {
      include: [Comment, UserMessageRating],
      order: [
        ["id", "ASC"]
      ]
    })
    .then(message => {
      setMessagesRating(message);
      res.send(message).status(200);
    })
    .catch(error => {
      res.send(error).status(500);
    });
});

function validateSearchParameter(searchParameter) {
  if (searchParameter == undefined || searchParameter == null) return false;

  return true;
}

function setMessagesRating(messages) {
  if (Array.isArray(messages)) {
    messages.forEach(message => {
      addRatingFieldToMessageWithRating(message);
    });
  } else {
    addRatingFieldToMessageWithRating(messages);
  }

  return messages;
}

function addRatingFieldToMessageWithRating(message) {
  const messageRatings = message.user_message_ratings;

  var numberOfLikes = messageRatings.filter(
    userMessageRating => userMessageRating.liked == true
  ).length;
  var numberOfDislikes = messageRatings.filter(
    userMessageRating => userMessageRating.disliked == true
  ).length;

  var numberOfLikesDislikes = {
    likes: numberOfLikes,
    dislikes: numberOfDislikes
  };

  message.rating = numberOfLikesDislikes;
}

//add new messages
router.post("/", passport.authenticate('jwt', {
  session: false
}), (req, res) => {
  Message.create(req.body)
    .then(result => {
      res.send(result).status(200);
    })
    .catch(error => {
      res.send(error).status(500);
    });
});

//edit existing messages
router.put("/:id", passport.authenticate('jwt', {
  session: false
}), (req, res) => {
  Message.update(req.body, {
      where: {
        id: req.params.id
      }
    })
    .then(result => {
      res.send(result).status(200);
    })
    .catch(err => {
      res.send(err).status(500);
    });
});

//delete message
router.delete("/:id", passport.authenticate('jwt', {
  session: false
}), (req, res) => {
  Message.destroy({
    where: {
      id: req.params.id
    }
  }).then(result => {
    if (result) {
      res.send("deleted").status(200);
    } else {
      res.send("something went wrong!").status(404);
    }
  }).catch(error => {
    res.send(error).status(500);
  });
});

//add/update rating for message
router.post("/addMessageRating", passport.authenticate('jwt', {
  session: false
}), (req, res) => {
  //check if there is already a rating with the same userId and messageId
  UserMessageRating.findOne({
    where: {
      messageId: req.body.messageId,
      userId: req.body.userId
    }
  }).then(userMessageRating => {
    if (userMessageRating != null) {
      userMessageRating
        .update(req.body)
        .then(result => {
          res.send(result);
        })
        .catch(error => {
          res.send(error).status(500);
        });
    } else {
      UserMessageRating.create(req.body)
        .then(result => {
          res.send(result).status(200);
        })
        .catch(error => {
          res.send(error).status(500);
        });
    }
  });
});

module.exports = router;