var express = require('express');
var passport = require('passport');
var router = express.Router();

var Bug = require('../models').bug;

//get all bugs
router.get('/', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    Bug.findAll({
        order: [
            ['id', 'ASC']
        ]
    }).then(bug => {
        res.send(bug).status(200);
    }).catch(error => {
        res.send(error).status(500);
    });
});

//add bug
router.post('/', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    Bug.create(req.body).then(result => {
        res.send(result).status(200);
    }).catch(error => {
        res.send(error).status(500);
    });
});

//delete bug
router.delete('/:id', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    Bug.destroy({
        where: {
            id: req.params.id
        }
    }).then(result => {
        if (result == 1)
            res.send('deleted!').status(200);
        else
            res.send('something went wrong!').status(404);
    }).catch(error => {
        res.send(error).status(500);
    });
});

module.exports = router;