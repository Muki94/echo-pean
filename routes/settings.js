var express = require('express');
var passport = require('passport');
var router = express.Router();

var Setting = require('../models').setting;

//get all settings
router.get('/', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    Setting.findAll({
        order: [
            ['id', 'ASC']
        ]
    }).then(settings => {
        res.send(settings).status(200);
    }).catch(error => {
        res.send(error).status(500);
    });
});

//get single setting
router.get('/:id', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    Setting.findById(req.params.id).then(setting => {
        res.send(setting).status(200);
    }).catch(error => {
        res.send(error).status(500);
    });
});

//add new setting
router.post('/', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    Setting.create(req.body).then(result => {
        res.send(result).status(200);
    }).catch(error => {
        res.send(error).status(500);
    });
});

//edit existing setting
router.put('/:id', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    Setting.update(req.body, {
        where: {
            id: req.params.id
        }
    }).then(result => {
        res.send(result).status(200);
    }).catch(err => {
        res.send(err).status(500);
    });
});

//delete setting
router.delete('/:id', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    Setting.destroy({
        where: {
            id: req.params.id
        }
    }).then(result => {
        if (result == 1)
            res.send('deleted!').status(200);
        else
            res.send('something went wrong!').status(404);
    }).catch(error => {
        res.send(error).status(500);
    });
});

module.exports = router;