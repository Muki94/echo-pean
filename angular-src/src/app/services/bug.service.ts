import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { JwtToken } from '../models/jwt-token';
import { Bug } from '../models/bug';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: "root"
})
export class BugService {
  url = "https://echooo.herokuapp.com/";

  constructor(private http: HttpClient) {
    console.log("Bug service started...");
  }

  // get all
  GetAllBugs(): Observable<Bug[]> {
    return this.http.get<Bug[]>(this.url + "bugs", JwtToken.getHeaders());
  }

  // add
  AddNewBug(bug: Bug) {
    return this.http.post(this.url + "bugs", bug, JwtToken.getHeaders());
  }

  // delete
  DeleteBug(id) {
    return this.http.delete(this.url + "bugs/" + id, JwtToken.getHeaders());
  }
}
