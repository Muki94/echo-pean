import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Message } from "@angular/compiler/src/i18n/i18n_ast";
import { FilterAttributes } from "../models/filter-attributes";
import { JwtToken } from '../models/jwt-token';

@Injectable({
  providedIn: "root"
})
export class MessageService {
  url = "https://echooo.herokuapp.com/";

  constructor(private http: HttpClient) {
    console.log("Message service started...");
  }

  //get all
  GetAllMessages(searchFilter: FilterAttributes): Observable<Message[]> {
    let newUrl = this.url + "messages?";

    if (searchFilter != null) {
      newUrl += searchFilter.content != null
        ? "content=" + searchFilter.content
        : "";
      newUrl += searchFilter.destination != null
        ? "destination=" + searchFilter.destination
        : "";
    }

    return this.http.get<Message[]>(newUrl, JwtToken.getHeaders());
  }

  //get single
  GetSingleMessage(id): Observable<Message> {
    return this.http.get<Message>(this.url + "messages/" + id, JwtToken.getHeaders());
  }

  //add
  //update
  //delete
}
