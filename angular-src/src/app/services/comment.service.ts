import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { JwtToken } from '../models/jwt-token';
import { Observable } from 'rxjs';
import { Comment } from '../models/comment';

@Injectable({
  providedIn: "root"
})
export class CommentService {
  url = "https://echooo.herokuapp.com/";

  constructor(private http: HttpClient) {
    console.log("Comment service started...");
  }

  //get all
  GetAllComments(): Observable<Comment[]> {
    return this.http.get<Comment[]>(this.url + "comments", JwtToken.getHeaders());
  }

  //get single
  GetSingleComment(id): Observable<Comment> {
    return this.http.get<Comment>(this.url + "comments/" + id, JwtToken.getHeaders());
  }
  //add
  //update
  //delete
}
