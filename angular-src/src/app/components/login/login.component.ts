import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { UserService } from "src/app/services/user.service";
import { SidebarService } from "src/app/services/sidebar.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  success = false;

  constructor(
    private userService: UserService,
    public sidebar: SidebarService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.loginForm = this.formBuilder.group({
      username: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  ngOnInit() {
    this.sidebar.hide();
  }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.authenticateUser({
      username: this.loginForm.controls.username.value,
      password: this.loginForm.controls.password.value
    });

    this.success = true;
  }

  authenticateUser(user) {
    this.userService.AuthenticateUser(user).subscribe(res => {
      if (res.success) {
        localStorage.setItem("id_token", res.token);
        localStorage.setItem("user", JSON.stringify(res.user));
        this.router.navigate(["/main"]);
      } else {
        this.router.navigate(["/login"]);
      }
    });
  }
}
