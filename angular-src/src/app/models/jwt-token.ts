import { User } from "./user";
import { HttpHeaders } from '@angular/common/http';

export class JwtToken {
  success: boolean;
  message: string;
  token: string;
  user: User;
  static getHeaders = function () {
    return {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `${localStorage.getItem("id_token")}`
      })
    };
  };
}
