import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

//components
import { AppComponent } from "./app.component";
import { LoginComponent } from "./components/login/login.component";
import { MainComponent } from "./components/main/main.component";
import { MyMessagesComponent } from "./components/my-messages/my-messages.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { RegisterComponent } from "./components/register/register.component";
import { SettingsComponent } from "./components/settings/settings.component";
import { SidebarComponent } from "./components/sidebar/sidebar.component";
import { StatisticsComponent } from "./components/statistics/statistics.component";

//services
import { MessageService } from "./services/message.service";
import { CommentService } from "./services/comment.service";
import { UserService } from "./services/user.service";
import { SettingService } from "./services/setting.service";
import { BugService } from "./services/bug.service";
import { SidebarService } from "./services/sidebar.service";
import { AuthGuardService as AuthGuard } from "./services/auth-guard.service";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    MyMessagesComponent,
    ProfileComponent,
    RegisterComponent,
    SettingsComponent,
    SidebarComponent,
    StatisticsComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    MessageService,
    CommentService,
    UserService,
    SettingService,
    BugService,
    SidebarService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
