const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const path = require('path');
const app = express();

const port = process.env.PORT || 3000;

app.use(express.static(__dirname + "/public/echoo-angular-src"))
app.use(cors());

//use body parser to enable post and put reqests 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

//passport middleware to implement token system
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

//include api-s 
app.use('/users', require('./routes/users'));
app.use('/messages', require('./routes/messages'));
app.use('/comments', require('./routes/comments'));
app.use('/settings', require('./routes/settings'));
app.use('/bugs', require('./routes/bugs'));


app.listen(port, function () {
  console.log(`server started on port ${port}`);
});

app.get("/*", function (req, res) {
  res.sendFile(path.join(__dirname + '/public/echoo-angular-src/index.html'));
});