'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('user_settings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'users',
          key: 'id'
        },
        validate: {
          notNull: true,
          isInt: true,
          min: 1
        },
        onDelete: 'CASCADE'
      },
      settingId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'settings',
          key: 'id'
        },
        validate: {
          notNull: true,
          isInt: true,
          min: 1
        },
        onDelete: 'CASCADE'
      },
      value: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('user_settings');
  }
};